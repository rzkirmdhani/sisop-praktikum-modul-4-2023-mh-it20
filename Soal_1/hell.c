#define FUSE_USE_VERSION 30
#define _XOPEN_SOURCE 500

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <crypt.h>

static const char *folderPath = "./yanuareko/soal1/data/gallery";
static const char *disableAreaPath = "./yanuareko/soal1/data/disable-area";
static const char *passwordFilePath = "./yanuareko/soal1/data/disable-area/password.txt";

// Fungsi untuk menghasilkan nama file yang telah dibalik
void reverseFileName(const char *inputFileName, char *outputFileName) {
    int length = strlen(inputFileName);

    for (int i = 0; i < length; i++) {
        outputFileName[i] = inputFileName[length - i - 1];
    }

    outputFileName[length] = '\0';
}

// Fungsi untuk memeriksa apakah password yang dimasukkan sesuai
static int checkPassword(const char *password) {
    FILE *passwordFile = fopen(passwordFilePath, "r");
    if (passwordFile == NULL) {
        return 0; // Gagal membuka file password
    }

    char storedPassword[100];
    fscanf(passwordFile, "%s", storedPassword);
    fclose(passwordFile);

    // Gunakan fungsi crypt() untuk memeriksa kesesuaian password
    char *encryptedPassword = crypt(password, storedPassword);
    return strcmp(encryptedPassword, storedPassword) == 0;
}

// Fungsi untuk memeriksa apakah path berada di dalam "disable-area" dan memerlukan password
static int isPasswordRequired(const char *path) {
    return strncmp(path, "/disable-area/", 14) == 0;
}

// Implementasi fungsi getattr untuk mendapatkan informasi atribut file atau direktori
static int my_getattr(const char *path, struct stat *stbuf) {
    int res = 0;
    char fullPath[100];
    sprintf(fullPath, "%s%s", folderPath, path);

    res = lstat(fullPath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

// Implementasi fungsi readdir untuk membaca isi direktori
static int my_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                       off_t offset, struct fuse_file_info *fi) {
    DIR *dp;
    struct dirent *de;

    (void) offset;
    (void) fi;

    char fullPath[100];
    sprintf(fullPath, "%s%s", folderPath, path);

    dp = opendir(fullPath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        if (filler(buf, de->d_name, NULL, 0))
            break;
    }

    closedir(dp);
    return 0;
}

// Implementasi fungsi rename untuk melakukan pembalikan nama file
static int my_rename(const char *oldpath, const char *newpath) {
    char oldFullPath[100];
    char newFullPath[100];
    sprintf(oldFullPath, "%s%s", folderPath, oldpath);
    sprintf(newFullPath, "%s%s", folderPath, newpath);

    int res = rename(oldFullPath, newFullPath);
    if (res == -1)
        return -errno;

    return 0;
}

// Implementasi fungsi unlink untuk menghapus file
static int my_unlink(const char *path) {
    char fullPath[100];
    sprintf(fullPath, "%s%s", folderPath, path);

    int res = unlink(fullPath);
    if (res == -1)
        return -errno;

    return 0;
}

// Fungsi untuk mengubah permission pada file "script.sh"
static int changeScriptPermission(const char *path) {
    char fullPath[100];
    sprintf(fullPath, "%s%s", folderPath, path);

    // Ubah permission file "script.sh" agar tidak dapat dijalankan
    int res = chmod(fullPath, 0644); // Permission: rw-r--r--
    if (res == -1)
        return -errno;

    return 0;
}

// Fungsi untuk membuat file dengan prefix "test" yang mengalami pembalikan isi
static int createAndReverseTestFile(const char *path) {
    char fullPath[100];
    sprintf(fullPath, "%s%s", folderPath, path);

    // Buat file dengan prefix "test"
    FILE *file = fopen(fullPath, "w");
    if (file == NULL)
        return -errno;

    // Tulis isi file yang di-reverse
    const char *content = "This is a test file.";
    char reversedContent[100];
    reverseFileName(content, reversedContent);
    fprintf(file, "%s", reversedContent);

    fclose(file);

    return 0;
}

// Fungsi untuk menangani berkas teks dengan prefix tertentu
static int handleTextFile(const char *path, const char *prefix) {
    char fullPath[100];
    sprintf(fullPath, "%s%s", folderPath, path);

    FILE *file = fopen(fullPath, "r");
    if (file == NULL)
        return -errno;

    // Baca isi file
    fseek(file, 0, SEEK_END);
    long fileSize = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *fileContent = (char *)malloc(fileSize + 1);
    fread(fileContent, 1, fileSize, file);
    fclose(file);
    fileContent[fileSize] = '\0';

    // Proses file berdasarkan prefix
    if (strncmp(prefix, "base64", 6) == 0) {
        // Decode dengan algoritma Base64
        BIO *bio, *b64;
        FILE *stream;
        int encodedSize = strlen(fileContent);
        char *decodedContent = (char *)malloc(encodedSize);

        b64 = BIO_new(BIO_f_base64());
        bio = BIO_new_mem_buf(fileContent, -1);
        bio = BIO_push(b64, bio);

        BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
        BIO_read(bio, decodedContent, encodedSize);

        BIO_free_all(bio);

        // Simpan hasil decoding ke file
        FILE *decodedFile = fopen(fullPath, "w");
        fprintf(decodedFile, "%s", decodedContent);
        fclose(decodedFile);

        free(decodedContent);
    } else if (strncmp(prefix, "rot13", 5) == 0) {
        // Decode dengan algoritma ROT13
        for (int i = 0; i < fileSize; i++) {
            if ((fileContent[i] >= 'A' && fileContent[i] <= 'Z') || (fileContent[i] >= 'a' && fileContent[i] <= 'z')) {
                if (fileContent[i] >= 'A' && fileContent[i] <= 'Z') {
                    fileContent[i] = ((fileContent[i] - 'A' + 13) % 26) + 'A';
                } else {
                    fileContent[i] = ((fileContent[i] - 'a' + 13) % 26) + 'a';
                }
            }
        }

        // Simpan hasil decoding ke file
        FILE *decodedFile = fopen(fullPath, "w");
        fprintf(decodedFile, "%s", fileContent);
        fclose(decodedFile);
    } else if (strncmp(prefix, "hex", 3) == 0) {
        // Decode dari representasi heksadesimal
        char *decodedContent = (char *)malloc(fileSize / 2);

        for (int i = 0, j = 0; i < fileSize; i += 2, j++) {
            sscanf(fileContent + i, "%2x", (unsigned int *)(decodedContent + j));
        }

        // Simpan hasil decoding ke file
        FILE *decodedFile = fopen(fullPath, "w");
        fprintf(decodedFile, "%s", decodedContent);
        fclose(decodedFile);

        free(decodedContent);
    } else if (strncmp(prefix, "rev", 3) == 0) {
        // Decode dengan membalikkan teks
        char *decodedContent = (char *)malloc(fileSize + 1);
        for (int i = 0; i < fileSize; i++) {
            decodedContent[i] = fileContent[fileSize - i - 1];
        }
        decodedContent[fileSize] = '\0';

        // Simpan hasil decoding ke file
        FILE *decodedFile = fopen(fullPath, "w");
        fprintf(decodedFile, "%s", decodedContent);
        fclose(decodedFile);

        free(decodedContent);
    }

    free(fileContent);
    return 0;
}

// Implementasi fungsi access untuk memeriksa akses ke "disable-area"
static int my_access(const char *path, int mask) {
    // Periksa apakah path berada di dalam "disable-area" dan memerlukan password
    if (isPasswordRequired(path)) {
        char password[100];
        printf("Enter the password: ");
        scanf("%s", password);

        // Periksa password
        if (!checkPassword(password)) {
            printf("Incorrect password\n");
            return -EACCES; // Akses ditolak
        }
    }

    // Jika bukan path "disable-area" atau password sesuai, lanjutkan dengan akses
    return access(path, mask);
}

// Implementasi fungsi utama FUSE
static struct fuse_operations my_oper = {
    .getattr = my_getattr,
    .readdir = my_readdir,
    .rename = my_rename,
    .unlink = my_unlink,
    .create = createAndReverseTestFile, // Tambahkan fungsi untuk membuat file
    .read = my_read, // Tambahkan fungsi untuk membaca file
    .access = my_access, // Tambahkan fungsi untuk memeriksa akses
};

int main(int argc, char *argv[]) {
    // Jalankan FUSE
    return fuse_main(argc, argv, &my_oper, NULL);
}
