# sisop-praktikum-modul-4-2023-MH-IT20
Laporan pengerjaan soal shift modul 3 Praktikum Sistem Operasi 2023 Kelompok IT20

## Anggota Kelompok:
| N0. | Nama Anggota | NRP |
|-----| ----------- | ----------- |
| 1. | M. Januar Eko Wicaksono | 5027221006 |
| 2. | Rizki Ramadhani | 5027221013 |
| 3. |Khansa Adia Rahma | 5027221071 | 

## Soal 1

### Study case soal 1
---
Anthoni Salim merupakan seorang pengusaha yang memiliki supermarket terbesar di Indonesia. Ia sedang melakukan inovasi besar dalam dunia bisnis ritelnya. Salah satu ide cemerlang yang sedang dia kembangkan adalah terkait dengan pengelolaan foto dan gambar produk dalam sistem manajemen bisnisnya. Anthoni bersama sejumlah rekan bisnisnya sedang membahas konsep baru yang akan mengubah cara produk-produk di supermarketnya dipresentasikan dalam katalog digital. Untuk resource dapat di download pada link ini.

### Problem
---
a Pada folder “gallery”, agar katalog produk lebih menarik dan kreatif, Anthoni dan tim memutuskan untuk:

- Membuat folder dengan prefix "rev." Dalam folder ini, setiap gambar yang dipindahkan ke dalamnya akan mengalami pembalikan nama file-nya. 
```c
Ex: "mv EBooVNhNe7tU7q08jgTe.HEIC rev-test/" 
Output: eTgj80q7Ut7eNhNVooBE.HEIC
```

- Anthoni dan timnya ingin menghilangkan gambar-gambar produk yang sudah tidak lagi tersedia dengan membuat folder dengan prefix "delete." Jika sebuah gambar produk dipindahkan ke dalamnya, nama file-nya akan langsung terhapus.
```c
Ex: "mv coba-deh.jpg delete-foto/" 
```

b. Pada folder "sisop," terdapat file bernama "script.sh." Anthoni dan timnya menyadari pentingnya menjaga keamanan dan integritas data dalam folder ini.

- Mereka harus mengubah permission pada file "script.sh" karena jika dijalankan maka dapat menghapus semua dan isi dari  "gallery," "sisop," dan "tulisan."

-  Anthoni dan timnya juga ingin menambahkan fitur baru dengan membuat file dengan prefix "test" yang ketika disimpan akan mengalami pembalikan (reverse) isi dari file tersebut.  

c. Pada folder "tulisan" Anthoni ingin meningkatkan kemampuan sistem mereka dalam mengelola berkas-berkas teks dengan menggunakan fuse.
- Jika sebuah file memiliki prefix "base64," maka sistem akan langsung mendekode isi file tersebut dengan algoritma Base64.

- Jika sebuah file memiliki prefix "rot13," maka isi file tersebut akan langsung di-decode dengan algoritma ROT13.

- Jika sebuah file memiliki prefix "hex," maka isi file tersebut akan langsung di-decode dari representasi heksadesimalnya.

- Jika sebuah file memiliki prefix "rev," maka isi file tersebut akan langsung di-decode dengan cara membalikkan teksnya.

d. Pada folder “disable-area”, Anthoni dan timnya memutuskan untuk menerapkan kebijakan khusus. Mereka ingin memastikan bahwa folder dengan prefix "disable" tidak dapat diakses tanpa izin khusus.

-  Jika seseorang ingin mengakses folder dan file pada “disable-area”, mereka harus memasukkan sebuah password terlebih dahulu (password bebas).

e. Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format :
```c
[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]
Ex:
[SUCCESS]::01/11/2023-10:43:43::rename::Move from /gallery/DuIJWColl2UYknZ8ubz6.HEIC to /gallery/foto/DuIJWColl2UYknZ8ubz6

### Solution
---
[Source Code](./Soal_2)


### Kendala
---
1. Tidak ada kendala apapun.

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Tidak ada, pada saat demo sudah lancar dan tidak ada tambahan dari aslab penguji

### Hasil
---

1. Hasil Run belajar.c `belajar.c`

![hasil File 'belajar.c'](img/Soal_1/1.png)

2. Hasil Run yang.c `yang.c`

![hasil File 'yang.c'](img/Soal_1/2.png)

3. Hasil Run rajin.c `rajin.c`

![hasil File 'rajin.c'](img/Soal_1/3.png)


## Soal 2

### Study case soal 2
---
Manda adalah seorang mahasiswa IT, dimana ia merasa hari ini adalah hari yang menyebalkan karena sudah bertemu lagi dengan praktikum sistem operasi. Pada materi hari ini, ia mempelajari suatu hal bernama FUSE. Karena sesi lab telah selesai, kini waktunya untuk mengerjakan penugasan praktikum. Manda mendapatkan firasat jika soal modul kali ini adalah yang paling sulit dibandingkan modul lainnya, sehingga dia akan mulai mengerjakan tugas praktikum-nya. Sebelumnya, Manda mendapatkan file yang perlu didownload secara manual terlebih dahulu pada link ini. Selanjutnya, ia tinggal mengikuti langkah - langkah yang diminta untuk mengerjakan soal ini hingga akhir. Manda berharap ini menjadi modul terakhir sisop yang ia pelajari

### Problem
---
a Membuat file open-password.c untuk membaca file zip-pass.txt

- Melakukan proses dekripsi base64 terhadap file tersebut

- Lalu unzip home.zip menggunakan password hasil dekripsi

b. Membuat file semangat.c

- Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format :
```c
**[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]
Ex:**
[SUCCESS]::06/11/2023-16:05:48::readdir::Read directory /sisop
```

- Selanjutnya untuk memudahkan pengelolaan file, Manda diminta untuk mengklasifikasikan file sesuai jenisnya:
    - Melakukan unzip pada home.zip
    - Membuat folder yang dapat langsung memindahkan file dengan kategori ekstensi file yang mereka tetapkan:
        - documents: .pdf dan .docx. 
        - images: .jpg, .png, dan .ico. 
        - website: .js, .html, dan .json. 
        - sisop: .c dan .sh. 
        - text: .txt. 
        - aI: .ipynb dan .csv.

c. Karena sedang belajar tentang keamanan, tiap kali mengakses file (cat nama-file) pada di dalam folder text maka perlu memasukkan password terlebih dahulu

- Password terdapat di file password.bin

d. Pada folder website

- Membuat file csv pada folder website dengan format ini: file,title,body

- Tiap kali membaca file csv dengan format yang telah ditentukan, maka akan langsung tergenerate sebuah file html sejumlah yang telah dibuat pada file csv

- Tidak dapat menghapus file / folder yang mengandung prefix “restricted”

- Pada folder documents
    - Karena ingin mencatat hal - hal penting yang mungkin diperlukan, maka Manda diminta untuk menambahkan detail - detail kecil dengan memanfaatkan attribut

e. Membuat file server.c

- Pada folder ai, terdapat file webtoon.csv

- Manda diminta untuk membuat sebuah server (socket programming) untuk 
membaca webtoon.csv. Dimana terjadi pengiriman data antara client ke server dan server ke client.
    - Menampilkan seluruh judul
    - Menampilkan berdasarkan genre
    - Menampilkan berdasarkan hari
    - Menambahkan ke dalam file webtoon.csv
    - Melakukan delete berdasarkan judul
    - Selain command yang diberikan akan menampilkan tulisan “Invalid Command”

- Manfaatkan client.c pada folder sisop sebagai client 

### Solution
---
[Source Code](./Soal_1)


### Kendala
---
1. Tidak ada kendala apapun.

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Tidak ada, pada saat demo sudah lancar dan tidak ada tambahan dari aslab penguji

### Hasil
---

1. Hasil Run belajar.c `belajar.c`

![hasil File 'belajar.c'](img/Soal_1/1.png)

2. Hasil Run yang.c `yang.c`

![hasil File 'yang.c'](img/Soal_1/2.png)

3. Hasil Run rajin.c `rajin.c`

![hasil File 'rajin.c'](img/Soal_1/3.png)

## Soal 3

### Study case soal 3
---
Dalam kegelapan malam yang sepi, bulan purnama menerangi langit dengan cahaya peraknya. Rumah-rumah di sekitar daerah itu terlihat seperti bayangan yang menyelimuti jalan-jalan kecil yang sepi. Suasana ini terasa aneh, membuat hati seorang wanita bernama Sarah merasa tidak nyaman.

Dia berjalan ke jendela kamarnya, menatap keluar ke halaman belakang. Pohon-pohon besar di halaman tampak gelap dan menyeramkan dalam sinar bulan. Sesekali, cahaya bulan yang redup itu memperlihatkan bayangan-bayangan aneh yang bergerak di antara pepohonan.

Tiba-tiba, Ting! Pandangannya tertuju pada layar smartphonenya. Dia terkejut bukan kepalang. Notifikasi barusan adalah remainder pengumpulan praktikum yang dikirim oleh asisten praktikumnya. Sarah lupa bahwa 2 jam lagi adalah waktu pengumpulan tugas praktikum mata kuliah Sistem Operasi. Sarah sejenak merasa putus asa, saat dia menyadari bahwa ketidaknyamanan yang dirasakannya adalah akibat tekanan tugas praktikumnya.

Sarah melihat tugas praktikum yang harus dia selesaikan, dan dalam hitungan detik, rasa panik melanda. Tugas ini tampaknya sangat kompleks, dan dia belum sepenuhnya siap. Sebagai seorang mahasiswa teknik komputer, mata kuliah Sistem Operasi adalah salah satu mata kuliah yang sangat menuntut, dan dia harus menyelesaikan tugas ini dengan baik untuk menjaga nilai akademiknya.


### Problem
---
Tugas yang diberikan adalah untuk membuat sebuah filesystem dengan ketentuan sebagai berikut:

- Pada filesystem tersebut, jika User membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 

- Jika sebuah direktori adalah direktori modular, maka akan dilakukan modularisasi pada folder tersebut dan juga sub-direktorinya.

- Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan berikut:
    - Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
    - Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
    - Format untuk logging yaitu sebagai berikut.
    **[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]**  
    
- **Contoh:**
REPORT::231105-12:29:28::RENAME::/home/sarah/selfie.jpg::/home/sarah/cantik.jpg
REPORT::231105-12:29:33::CREATE::/home/sarah/test.txt
FLAG::231105-12:29:33::RMDIR::/home/sarah/folder

- Saat dilakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
**Contoh:**
file File_Sarah.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Sarah.txt.000, File_Sarah.txt.001, dan File_Sarah.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).

- Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

Contoh:
- /tmp/test_fuse/ adalah filesystem yang harus dirancang.
- /home/index/modular/ adalah direktori asli.

### Solution
---
```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdarg.h>
#include <stdlib.h>
#include <regex.h>

static const char *dirpath = "/home/dhani/modular";

void log_command(const char *level, const char *cmd, const char *desc, ...)
{
    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    FILE *log_file = fopen("/home/dhani/fs_module.log", "a");
    if (log_file != NULL)
    {
        va_list args;
        va_start(args, desc);

        fprintf(log_file, "[%s]::%s::%s::", level, timestamp, cmd);
        vfprintf(log_file, desc, args);
        fprintf(log_file, "\n");

        va_end(args);

        fclose(log_file);
    }
}

void modularize_file(const char *path)
{
    FILE *input_file = fopen(path, "rb");
    if (input_file == NULL)
    {
        return;
    }

    fseek(input_file, 0, SEEK_END);
    long file_size = ftell(input_file);
    rewind(input_file);

    if (file_size <= 1024)
    {
        fclose(input_file);
        return;
    }

    int count = (file_size + 1023) / 1024;

    for (int i = 1; i <= count; i++)
    {
        char chunk_path[256];
        snprintf(chunk_path, sizeof(chunk_path), "%s.%03d", path, i);
        FILE *output_file = fopen(chunk_path, "wb");

        if (output_file == NULL)
        {
            fclose(input_file);
            return;
        }

        char *buffer = (char *)malloc(1024);
        if (buffer == NULL)
        {
            fclose(input_file);
            fclose(output_file);
            return;
        }

        size_t bytes = fread(buffer, 1, 1024, input_file);
        fwrite(buffer, 1, bytes, output_file);

        free(buffer);
        fclose(output_file);
    }

    fclose(input_file);
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
    {
        return -errno;
    }

    return 0;
}

int dot_3_digit(const char *filename)
{
    const char *pattern = ".*\\.[0-9]{3}$";
    regex_t regex;

    if (regcomp(&regex, pattern, REG_EXTENDED))
        return 0;

    int match = !regexec(&regex, filename, 0, NULL, 0);
    regfree(&regex);

    return match;
}

void deleteFiles(const char *path)
{
    DIR *dir = opendir(path);

    if (dir == NULL)
    {
        perror("Error opening directory");
        return;
    }

    struct dirent *entry;

    while ((entry = readdir(dir)) != NULL)
    {
        if (dot_3_digit(entry->d_name))
        {
            char full_path[1000];
            sprintf(full_path, "%s/%s", path, entry->d_name);

            if (remove(full_path) != 0)
            {
                perror("Error deleting file");
            }
            else
            {
                printf("Deleted file: %s\n", full_path);
            }
        }
    }

    closedir(dir);
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
    {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
    {
        return -errno;
    }

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char full_path[1256];
        sprintf(full_path, "%s/%s", fpath, de->d_name);

        if (strstr(fpath, "/module_"))
        {
            if (de->d_type == DT_REG && !dot_3_digit(de->d_name))
            {
                res = filler(buf, de->d_name, &st, 0);
                if (res != 0)
                {
                    break;
                }
                modularize_file(full_path);
            }
        }
        else
        {
            if (de->d_type == DT_DIR || de->d_type == DT_REG)
            {
                res = filler(buf, de->d_name, &st, 0);
                if (res != 0)
                {
                    break;
                }
                deleteFiles(fpath);
            }
        }
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;

        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1)
    {
        return -errno;
    }

    res = pread(fd, buf, size, offset);

    if (res == -1)
    {
        res = -errno;
    }

    close(fd);

    return res;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = open(fpath, fi->flags, mode);

    if (res == -1)
    {
        log_command("REPORT", "CREATE", "Error creating file %s", fpath);
        return -errno;
    }

    fi->fh = res;

    log_command("REPORT", "CREATE", fpath);

    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = fi->fh;
    int res = pwrite(fd, buf, size, offset);

    if (res == -1)
    {
        log_command("REPORT", "WRITE", "Error writing to file %s", fpath);
        return -errno;
    }

    log_command("REPORT", "WRITE", "Write to file %s", fpath);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);

    if (res == -1)
    {
        log_command("REPORT", "MKDIR", "Error creating directory %s", fpath);
        return -errno;
    }

    log_command("REPORT", "MKDIR", "Directory created: %s", fpath);

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    char from_path[1000];
    char to_path[1000];
    sprintf(from_path, "%s%s", dirpath, from);
    sprintf(to_path, "%s%s", dirpath, to);

    int res = rename(from_path, to_path);

    if (res == -1)
    {
        log_command("REPORT", "RENAME", "Error renaming %s to %s", from_path, to_path);
        return -errno;
    }

    log_command("REPORT", "RENAME", "%s to %s", from_path, to_path);

    return 0;
}

static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = rmdir(fpath);

    if (res == -1)
    {
        log_command("FLAG", "RMDIR", "Error removing directory %s", fpath);
        return -errno;
    }

    log_command("FLAG", "RMDIR", fpath);

    return 0;
}

static int xmp_unlink(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = unlink(fpath);

    if (res == -1)
    {
        log_command("REPORT", "UNLINK", "Error unlinking file %s", fpath);
        return -errno;
    }

    log_command("REPORT", "UNLINK", fpath);

    return 0;
}

static int xmp_utimens(const char *path, const struct timespec tv[2])
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = utimensat(0, fpath, tv, AT_SYMLINK_NOFOLLOW);
    if (res == -1)
    {
        return -errno;
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .create = xmp_create,
    .write = xmp_write,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .utimens = xmp_utimens,
};


int main(int argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```


### Kendala
---
1. Tidak ada kendala apapun.

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Tidak ada, pada saat demo sudah lancar dan tidak ada tambahan dari aslab penguji

### Hasil
---

1. Hasil Run belajar.c `belajar.c`

![hasil File 'belajar.c'](img/Soal_1/1.png)

2. Hasil Run yang.c `yang.c`

![hasil File 'yang.c'](img/Soal_1/2.png)

3. Hasil Run rajin.c `rajin.c`

![hasil File 'rajin.c'](img/Soal_1/3.png)
